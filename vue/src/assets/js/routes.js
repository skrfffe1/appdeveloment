import Dashboard from '../../components/pages/Dashboard';
import Books from '../../components/pages/Books';
import Patrons from '../../components/pages/Patrons';
import Settings from '../../components/pages/Settings';

export default[
    {path:'/', component:Dashboard},
    {path:'/dashboard', component:Dashboard},
    {path:'/books', component:Books},
    {path:'/patrons', component:Patrons},
    {path:'/settings', component:Settings},
]