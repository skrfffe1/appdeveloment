export const SET_BOOKS = (state, books) => {
    state.books = books
} 
export const SET_PATRONS = (state, patrons) => {
    state.patrons = patrons
} 
export const SET_CATEGORIES = (state, categories) => {
    state.categories = categories
}
export const ADD_PATRON = (state, patron) => {
    state.patrons.push(patron)
} 
export const UPDATE_PATRON = (state, updatePatron) => {
    let i = state.patrons.findIndex((patron) => patron.id === updatePatron.id);
    state.patrons.splice(i, 1, updatePatron);
}
export const DELETE_PATRON = (state, patronID) => {
    let filtered = state.patrons.filter((patron) => patron.id !== patronID);
    state.patrons = filtered;
}
export const ADD_BOOK = (state, book) => {
    state.books.push(book)
}
export const UPDATE_BOOK = (state, updatedBook) => {
    let i = state.books.findIndex((book) => book.id === updatedBook.id);
    state.books.splice(i, 1, updatedBook);
}