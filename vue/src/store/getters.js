export const getBooksData = (state) => {
    return state.books;
}

export const getPatronsData = (state) => {
    return state.patrons;
}

export const getCategoriesData = (state) => {
    return state.categories;
}