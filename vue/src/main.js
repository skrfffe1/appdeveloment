import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import routeLinks from './assets/js/routes'
import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";

import store from './store';

Vue.use(Toast, {
  transition: "Vue-Toastification__bounce",
  maxToasts: 5,
  newestOnTop: true
});

Vue.use(VueRouter)

const router = new VueRouter({
  routes: routeLinks,
  mode: 'history'
});

//----------------BOOTSTRAP----------------------------
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
//----------------BOOTSTRAP----------------------------

//style.css
import '../src/assets/css/style.css'


Vue.config.productionTip = false

new Vue({

  render: h => h(App),
  router: router,
  store

}).$mount('#app')
