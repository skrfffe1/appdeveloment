<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Resources\ReturnedBookResource;
use App\Models\ReturnedBook;

class ReturnedBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $returnedBook = ReturnedBook::paginate(10);
        return response()->json(ReturnedBookResource::collection($returnedBook));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $returnedBook = new ReturnedBook();
        $returnedBook->book_id = $request->book_id;
        $returnedBook->copies = $request->copies;
        $returnedBook->patron_id = $request->patron_id;

        if($returnedBook->save()){
            return response()->json(['message' => 'Book Returned!', 'returnedBook' => new ReturnedBookResource($returnedBook)]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $returnedBook = ReturnedBook::findOrFail($id);
        return response()->json(new ReturnedBookResource($returnedBook));
    }
}
