<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Resources\PatronResource;
use App\Models\Patron;

class PatronController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patron = Patron::paginate(10);
        return response()->json(PatronResource::collection($patron));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $patron = new Patron();
        $patron->last_name = $request->last_name;
        $patron->first_name = $request->first_name;
        $patron->middle_name = $request->middle_name;
        $patron->email = $request->email;

        if($patron->save()){
            return response()->json(['message' => 'Patron Saved', 'patron' => new PatronResource($patron)]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $patron = Patron::findOrFail($id);
        return response()->json(new PatronResource($patron));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $patron = Patron::findOrFail($id);
        $patron->last_name = $request->last_name;
        $patron->first_name = $request->first_name;
        $patron->middle_name = $request->middle_name;
        $patron->email = $request->email;
 
        if($patron->save()){
            return response()->json(['message' => 'Patron Updated', 'patron' => new PatronResource($patron)]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patron = Patron::findOrFail($id);
        
        if($patron->delete()){
            return response()->json(['message' => 'Patron Delete!', 'patron'=> new PatronResource($patron)]);
        }
    }
}
