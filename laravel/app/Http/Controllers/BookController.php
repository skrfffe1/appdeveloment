<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Resources\BookResource;
use App\Models\Book;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $book = Book::paginate(10);
        return response()->json(BookResource::collection($book));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $book = new Book();
        $book->name = $request->name;
        $book->author = $request->author;
        $book->copies = $request->copies;
        $book->category_id = $request->category_id;

        if($book->save()){
            return response()->json(['message' => 'Book Saved', 'book' => new BookResource($book)]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::findOrFail($id);
        return response()->json(new BookResource($book));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $book = Book::findOrFail($id);
        $book->name = $request->name;
        $book->author = $request->author;
        $book->copies = $request->copies;
        $book->category_id = $request->category_id;
 
        if($book->save()){
            return response()->json(['message' => 'Book Updated', 'book' => new BookResource($book)]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::findOrFail($id);
        
        if($book->delete()){
            return response()->json(['message' => 'Book Delete!', 'book'=> new BookResource($book)]);
        }
    }
}
