<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Resources\BorrowedBookResource;
use App\Models\BorrowedBook;

class BorrowedBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $borrowedBook = BorrowedBook::paginate(10);
        return response()->json(BorrowedBookResource::collection($borrowedBook));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $borrowedBook = new BorrowedBook();
        $borrowedBook->patron_id = $request->patron_id;
        $borrowedBook->copies = $request->copies;
        $borrowedBook->book_id = $request->book_id;

        if($borrowedBook->save()){
            return response()->json(['message' => 'Book Borrowed!', 'borrowedBook' => new BorrowedBookResource($borrowedBook)]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $borrowedBook = BorrowedBook::findOrFail($id);
        return response()->json(new BorrowedBookResource($borrowedBook));
    }

}
