<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    public function category(){
        return $this->hasOne(Category::class, 'id');
    }

    public function borrowedBook(){
        return $this->belongsTo(BorrowedBook::class);
    }

    public function returnedBook(){
        return $this->belongsTo(ReturnedBook::class);
    }
}
