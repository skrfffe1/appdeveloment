<?php

namespace Database\Factories;

use App\Models\Patron;
use Illuminate\Database\Eloquent\Factories\Factory;

use Illuminate\Support\Str;

class PatronFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Patron::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'last_name' => $this->faker->lastName,
            'first_name' => $this->faker->firstName,
            'middle_name' => strtoupper($this->faker->randomLetter),
            'email' => $this->faker->unique()->safeEmail
        ];
    }
}
