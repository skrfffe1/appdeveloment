<?php

namespace Database\Factories;

use App\Models\ReturnedBook;
use Illuminate\Database\Eloquent\Factories\Factory;

use Illuminate\Support\Str;

class ReturnedBookFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ReturnedBook::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'copies' => $this->faker->numberBetween($min = 1, $max = 250),
            'patron_id' => 1,
            'book_id' => 1
        ];
    }
}
