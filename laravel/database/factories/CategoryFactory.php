<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

use Illuminate\Support\Str;

class CategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $categories = ['Fiction', 'Horror', 'Sci-Fi', 'History'];

        return [
            'category' => $categories[$this->faker->numberBetween($min = 0, $max = 3)]
        ];
    }
}
