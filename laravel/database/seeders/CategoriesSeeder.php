<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Category;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ['Fiction', 'Horror', 'Sci-Fi', 'History'];

        for ($i=0; $i < count($categories); $i++) { 
            Category::create(['category' => $categories[$i]]);
        }
    }
}
