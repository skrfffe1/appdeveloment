<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Book;
use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Schema;

class BookTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    
    use RefreshDatabase, WithFaker;
    
    /** @test */
    public function books_table_has_expected_columns(){
        $this->assertTrue(
            Schema::hasColumns(
                'books',
                [
                    'id',
                    'created_at',
                    'updated_at',
                    'name',
                    'author',
                    'copies',
                    'category_id'
                ]
            )
        );
    }
    /** @test */
    public function book_has_a_category(){
        $category = Category::factory()->create(['category' => 'sampleCategory']);
        $book = Book::factory()->create(['category_id' => $category->id]);

        $this->assertInstanceOf(Category::class, $book->category);
    }
}
