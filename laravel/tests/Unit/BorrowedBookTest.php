<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\BorrowedBook;
use App\Models\Book;
use App\Models\Patron;
use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Schema;

class BorrowedBookTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */

    use RefreshDatabase, WithFaker;
    
    /** @test */
    public function borrowed_books_table_has_expected_columns(){
        $this->assertTrue(
            Schema::hasColumns(
                'borrowed_books',
                [
                    'id',
                    'created_at',
                    'updated_at',
                    'patron_id',
                    'copies',
                    'book_id'
                ]
            )
        );
    }
    /** @test */
    public function borrowed_books_has_many_books(){
        $category = Category::factory()->create();
        $book = Book::factory()->create(['category_id' => $category->id]);
        $patron = Patron::factory()->create();
        $borrowedBook = BorrowedBook::factory()->create(['book_id' => $book->id, 'patron_id' => $patron->id]);

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $borrowedBook->book);

    }
    /** @test */
    public function borrowed_books_has_many_patrons(){
        $patron = Patron::factory()->create();
        $category = Category::factory()->create();
        $book = Book::factory()->create(['category_id' => $category->id]);
        $borrowedBook = BorrowedBook::factory()->create(['patron_id' => $patron->id, 'book_id'=> $book->id]);

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $borrowedBook->patron);
    }
}
