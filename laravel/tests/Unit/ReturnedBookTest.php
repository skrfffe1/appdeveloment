<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\ReturnedBook;
use App\Models\Book;
use App\Models\Patron;
use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Schema;

class ReturnedBookTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */

    use RefreshDatabase, WithFaker;
    
    /** @test */
    public function returned_books_table_has_expected_columns(){
        $this->assertTrue(
            Schema::hasColumns(
                'returned_books',
                [
                    'id',
                    'created_at',
                    'updated_at',
                    'patron_id',
                    'copies',
                    'book_id'
                ]
            )
        );
    }
    /** @test */
    public function returned_books_has_many_books(){
        $category = Category::factory()->create();
        $book = Book::factory()->create(['category_id' => $category->id]);
        $patron = Patron::factory()->create();
        $returnedBook = ReturnedBook::factory()->create(['book_id' => $book->id, 'patron_id' => $patron->id]);

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $returnedBook->book);
    }
    /** @test */
    public function returned_books_has_many_patrons(){
        $patron = Patron::factory()->create();
        $category = Category::factory()->create();
        $book = Book::factory()->create(['category_id' => $category->id]);
        $returnedBook = ReturnedBook::factory()->create(['patron_id' => $patron->id, 'book_id'=> $book->id]);

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $returnedBook->patron);
    }
}
